﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject deathFX;
    [SerializeField] int score = 10;
    [SerializeField, Tooltip("Hits to kill (Multiple shots fired at once count as a hit each)")] int hitsToDestroy = 1;

    ScoreBoard scoreBoard;

    void Awake()
    {
        GetComponent<BoxCollider>().isTrigger = false; ;
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    void OnParticleCollision(GameObject other)
    {
        hitsToDestroy--;
        if (scoreBoard && hitsToDestroy == 0)
        {
            scoreBoard.AddScore(this.score);
            Instantiate(deathFX, this.transform.position, Quaternion.identity);
            gameObject.SendMessage("OnEnemyDestroyed");
            Destroy(this.gameObject);
        }
    }
}
