﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 2f;
    [SerializeField] GameObject deathFx;

    void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
    }

    void StartDeathSequence()
    {
        Invoke("ReloadLevel", levelLoadDelay);
        this.gameObject.SendMessage("OnPlayerDeath");
        deathFx.SetActive(true);
    }

    void ReloadLevel()
    {
        SceneManager.LoadScene(1);
    }
}
