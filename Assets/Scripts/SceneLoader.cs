﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 4f;

    void Start()
    {
        Invoke("LoadFirstScene", levelLoadDelay);
    }

    void LoadFirstScene()
    {
        SceneManager.LoadScene(1);
    }
}
