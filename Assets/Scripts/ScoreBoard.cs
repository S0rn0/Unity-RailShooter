﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

    int score = 0;
    Text scoreText;

	void Awake ()
    {
        scoreText = GetComponent<Text>();
        scoreText.text = this.score.ToString();
    }

    public void AddScore(int scoreIncrease)
    {
        this.score += scoreIncrease;
        this.score = Mathf.Clamp(this.score, 0, 99999999);
        scoreText.text = score.ToString();
    }
}
