﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed = 16f;
    [SerializeField] float xRange = 6.8f;
    [SerializeField] float yRange = 4f;

    [SerializeField] float positionPitchFactor = -3f;
    [SerializeField] float movementPitchFactor = -16f;

    [SerializeField] float positionYawFactor = 3f;

    [SerializeField] float movementRollFactor = -16f;

    [SerializeField] GameObject[] guns;

    bool isControlEnabled = true;

    void Update ()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    void ProcessTranslation()
    {
        float xOffset = CrossPlatformInputManager.GetAxis("Horizontal") * speed * Time.deltaTime;
        float yOffset = CrossPlatformInputManager.GetAxis("Vertical") * speed * Time.deltaTime;

        this.transform.localPosition = new Vector3(
            Mathf.Clamp(this.transform.localPosition.x + xOffset, -xRange, xRange),
            Mathf.Clamp(this.transform.localPosition.y + yOffset, -yRange, yRange),
            this.transform.localPosition.z);
    }

    void ProcessRotation()
    {
        float pitch = this.transform.localPosition.y * positionPitchFactor + CrossPlatformInputManager.GetAxis("Vertical") * movementPitchFactor;
        float yaw = this.transform.localPosition.x * positionYawFactor;
        float roll = CrossPlatformInputManager.GetAxis("Horizontal") * movementRollFactor;

        this.transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    void ProcessFiring()
    {

        foreach (GameObject gun in this.guns) {
            var em = gun.GetComponent<ParticleSystem>().emission;

            if (CrossPlatformInputManager.GetButton("Fire1"))
            {
                em.enabled = true;
            }
            else
            {
                em.enabled = false;
            }
        }
    }

    // Called by string reference (if name changes, change elsewhere)
    void OnPlayerDeath()
    {
        isControlEnabled = false;
    }
}
