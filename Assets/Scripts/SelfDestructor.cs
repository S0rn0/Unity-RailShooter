﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructor : MonoBehaviour
{
    [SerializeField, Tooltip("In second. If set to 0 or less, will not self destruct")] float timeToLive = 1f;

	void Start ()
    {
        if (timeToLive > Mathf.Epsilon)
        {
            Destroy(this.gameObject, timeToLive);
        }
    }
}
